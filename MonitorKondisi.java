import java.util.Scanner;

public class MonitorKondisi {
    
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Monitor Kondisi Tubuh =====");
        System.out.println("1. Hitung BMI");
        System.out.println("2. Hitung Berat Badan Ideal");
        System.out.print("Pilih menu (1-2): ");

        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                new HitungBMI().run();
                break;
            case 2:
                new HitungBeratBadanIdeal().run();
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
}
