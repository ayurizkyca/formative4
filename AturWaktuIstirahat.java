import java.util.Scanner;

class AturWaktuIstirahat {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Atur Waktu Istirahat =====");
        System.out.println("1. Atur Waktu Tidur");
        System.out.println("2. Atur Waktu Istirahat Pekan Ini");
        System.out.print("Pilih menu (1-2): ");

        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                new AturWaktuTidur().run();
                break;
            case 2:
                new AturWaktuIstirahatMingguan().run();
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
        scanner.close();
    }
}
