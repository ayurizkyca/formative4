import java.util.Scanner;

public class CekDenyutJantung {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Cek Denyut Jantung =====");
        System.out.print("Masukkan jumlah denyut jangtung (per menit): ");
        int jumlahDenyut = scanner.nextInt();

        if (isNormal(jumlahDenyut)) {
            tampilkanHasilDenyutJantung(jumlahDenyut);;
        } else {
            System.out.println("Denyut Jantung Tidak Normal");
        }
    }

    private boolean isNormal(int jumlahDenyut) {
        return (jumlahDenyut >= 60 && jumlahDenyut <= 100);
    }

    private void tampilkanHasilDenyutJantung(int jumlahDenyut) {
        System.out.println("===== Hasil Cek Denyut Jantung =====");
        System.out.println("Jumlah Denyut Jantung: " + jumlahDenyut);
        System.out.println("Denyut Jantung Normal");
    }
}
