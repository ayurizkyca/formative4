import java.util.Scanner;

public class HitungBMI {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Hitung BMI =====");
        System.out.print("Masukkan Berat Badan (kg): ");
        double beratBadan = scanner.nextDouble();
        System.out.print("Masukkan Tinggi Badan (m): ");
        double tinggiBadan = scanner.nextDouble();

        double bmi = hitungBMI(beratBadan, tinggiBadan);
        System.out.println("===== Hasil Perhitungan BMI =====");
        System.out.println("Berat Badan: " + beratBadan + " kg");
        System.out.println("Tinggi Badan: " + tinggiBadan + " m");
        System.out.println("BMI: " + bmi);

        tampilkanKategoriBMI(bmi);
    }

    private double hitungBMI(double beratBadan, double tinggiBadan) {
        return beratBadan / (tinggiBadan * tinggiBadan);
    }

    private void tampilkanKategoriBMI(double bmi) {
        System.out.println("Kategori BMI: " + tentukanKategoriBMI(bmi));
    }

    private String tentukanKategoriBMI(double BMI) {
        if (BMI <= 18) {
            return "Kamu kekurangan berat badan";
        } else if (BMI >= 18 && BMI < 29) {
            return "Kamu sehat";
        } else if (BMI >= 30 && BMI <= 42) {
            return "Kamu Overweight";
        } else {
            return "Tidak terdefinisikan";
        }
    }
}
