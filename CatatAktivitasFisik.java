import java.util.Scanner;

public class CatatAktivitasFisik {
    
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Catat Aktivitas Fisik =====");
        System.out.println("1. Catat Jarak Tempuh");
        System.out.println("2. Catat Durasi Latihan");
        System.out.print("Pilih menu (1-2): ");

        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                new CatatJarakTempuh().run();;
                break;
            case 2:
                new CatatDurasiLatihan().run();
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
}
