import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("===== Aplikasi Kesehatan =====");
            System.out.println("1. Monitor Kesehatan Tubuh");
            System.out.println("2. Catat Aktivitas Fisik");
            System.out.println("3. Saran Nutrisi");
            System.out.println("4. Atur Waktu Istirahat");
            System.out.println("5. Monitor Kondisi Tubuh");
            System.out.println("6. Keluar");
            System.out.print("Pilih menu (1-6): ");

            int pilihan = scanner.nextInt();

            switch (pilihan) {
                case 1:
                    new MonitorKesehatan().run();
                    break;
                case 2:
                    new CatatAktivitasFisik().run();
                    break;
                case 3:
                    new SaranNutrisi().run();
                    break;
                case 4:
                    new AturWaktuIstirahat().run();
                    break;
                case 5:
                    new MonitorKondisi().run(); 
                case 6:
                    System.out.println("Terima kasih. Aplikasi ditutup.");
                    System.exit(0);
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih lagi.");
            }
        }
    }
}




