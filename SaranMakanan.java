import java.util.Scanner;

public class SaranMakanan {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Rekomendasi Makanan Berdasarkan Nutrisi =====");
        System.out.println("Pilih jenis nutrisi:");
        System.out.println("1. Sumber Protein");
        System.out.println("2. Sumber Karbohidrat Kompleks");
        System.out.print("Masukkan pilihan (1/2): ");
        int pilihanNutrisi = scanner.nextInt();

        if (pilihanNutrisi == 1) {
            rekomendasiSumberProtein();
        } else if (pilihanNutrisi == 2) {
            rekomendasiSumberKarbohidratKompleks();
        } else {
            System.out.println("Pilihan tidak valid.");
        }
    }

    private void rekomendasiSumberProtein() {
        System.out.println("===== Rekomendasi Sumber Protein =====");
        System.out.println("1. Ayam");
        System.out.println("2. Ikan");
        System.out.println("3. Daging tanpa lemak");
        System.out.println("4. Telur");
       
        System.out.println("\nPastikan untuk mengonsumsi sumber protein dalam porsi seimbang.");
    }

    private void rekomendasiSumberKarbohidratKompleks() {
        System.out.println("===== Rekomendasi Sumber Karbohidrat Kompleks =====");
        System.out.println("1. Nasi merah");
        System.out.println("2. Roti gandum");
        System.out.println("3. Kentang");
        System.out.println("4. Quinoa");

        System.out.println("\nPastikan untuk mengonsumsi sumber karbohidrat kompleks dalam porsi seimbang.");
    }
}
