import java.util.Scanner;

public class CatatJarakTempuh {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Catat Jarak Tempuh =====");
        System.out.print("Masukkan jarak tempuh anda (jumlah langkah): ");
        int jarakTempuh = scanner.nextInt();

        if (isAktif(jarakTempuh)) {
            tampilkanHasilJarakTempuh(jarakTempuh);;
        } else {
            System.out.println("Anda Kurang Aktif, Perbanyak Aktifitas!");
        }

    }

    private boolean isAktif(int jumlahDenyut) {
        return (jumlahDenyut >= 7000);
    }

    private void tampilkanHasilJarakTempuh(int jarakTempuh) {
        System.out.println("===== Menyimpan Jarak Tempuh =====");
        System.out.println("Jarak tempuh anda: " + jarakTempuh);
        System.out.println("Anda Cukup Aktif, Tetap Semangat!");
    }
}
