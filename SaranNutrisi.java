import java.util.Scanner;

public class SaranNutrisi {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Saran Nutrisi =====");
        System.out.println("1. Saran Menu Makanan");
        System.out.println("2. Saran Minuman Sehat");
        System.out.print("Pilih menu (1-2): ");

        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                new SaranMakanan().run();
                break;
            case 2:
                new SaranMinuman().run();
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
}