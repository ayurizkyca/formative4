import java.util.Scanner;

public class SaranMinuman {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Rekomendasi Minuman Berdasarkan Nutrisi =====");
        System.out.println("Pilih jenis nutrisi:");
        System.out.println("1. Sumber Protein");
        System.out.println("2. Sumber Karbohidrat Kompleks");
        System.out.print("Masukkan pilihan (1/2): ");
        int pilihanNutrisi = scanner.nextInt();

        if (pilihanNutrisi == 1) {
            rekomendasiMinumanSumberProtein();
        } else if (pilihanNutrisi == 2) {
            rekomendasiMinumanSumberKarbohidratKompleks();
        } else {
            System.out.println("Pilihan tidak valid.");
        }
    }

    private void rekomendasiMinumanSumberProtein() {
        System.out.println("===== Rekomendasi Minuman Sumber Protein =====");
        System.out.println("1. Susu protein tinggi");
        System.out.println("2. Smoothie protein (dengan tambahan protein whey atau kacang-kacangan)");
        System.out.println("3. Minuman berbasis yogurt");
        System.out.println("4. Jus sayuran yang diperkaya protein (misalnya, jus bayam)");

        System.out.println("\nPastikan untuk mengonsumsi minuman sumber protein dalam porsi seimbang.");
    }

    private void rekomendasiMinumanSumberKarbohidratKompleks() {
        System.out.println("===== Rekomendasi Minuman Sumber Karbohidrat Kompleks =====");
        System.out.println("1. Air mineral");
        System.out.println("2. Teh herbal tanpa gula");
        System.out.println("3. Jus sayuran");
        System.out.println("4. Smoothie buah-buahan");

        System.out.println("\nPastikan untuk mengonsumsi minuman sumber karbohidrat kompleks dalam porsi seimbang.");
    }
}
