import java.util.Scanner;

public class HitungBeratBadanIdeal {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Hitung Berat Badan Ideal =====");
        System.out.print("Masukkan Tinggi Badan (cm): ");
        double tinggiBadanCm = scanner.nextDouble();

        double beratBadanIdeal = hitungBeratBadanIdeal(tinggiBadanCm);
        System.out.println("===== Hasil Perhitungan Berat Badan Ideal =====");
        System.out.println("Tinggi Badan: " + tinggiBadanCm + " cm");
        System.out.println("Berat Badan Ideal: " + beratBadanIdeal + " kg");
    }

    private double hitungBeratBadanIdeal(double tinggiBadanCm) {
        // Formula Broca: Berat Badan Ideal (kg) = (Tinggi Badan (cm) - 100) - (10% x (Tinggi Badan (cm) - 100))
        return (tinggiBadanCm - 100) - (0.1 * (tinggiBadanCm - 100));
    }
}
