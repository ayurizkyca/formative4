import java.util.Scanner;

public class MonitorKesehatan {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Monitor Kesehatan Tubuh =====");
        System.out.println("1. Cek Tekanan Darah");
        System.out.println("2. Cek Denyut Jantung");
        System.out.print("Pilih menu (1-2): ");

        int pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                new CekTekananDarah().run();;
                break;
            case 2:
                new CekDenyutJantung().run();;
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
}
