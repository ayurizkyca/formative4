import java.util.Scanner;

public class CekTekananDarah {
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Cek Tekanan Darah =====");
        System.out.print("Masukkan tekanan darah sistolik: ");
        int sistolik = scanner.nextInt();

        System.out.print("Masukkan tekanan darah diastolik: ");
        int diastolik = scanner.nextInt();

        if (isNormal(sistolik, diastolik)) {
            tampilkanHasilTekananDarah(sistolik, diastolik);
        } else {
            System.out.println("Tekanan Darah Tidak  Normal");
        }
    }

    private boolean isNormal(int sistolik, int diastolik) {
        return (sistolik >= 90 && diastolik >= 60 && sistolik <= 120 && diastolik <= 80 );
    }

    private void tampilkanHasilTekananDarah(int sistolik, int diastolik) {
        System.out.println("===== Hasil Cek Tekanan Darah =====");
        System.out.println("Tekanan Darah Sistolik: " + sistolik);
        System.out.println("Tekanan Darah Diastolik: " + diastolik);
        System.out.println("Tekanan Darah Normal");
    }
}
