import java.util.Scanner;

public class AturWaktuTidur {
    
    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Atur Waktu Tidur =====");
        System.out.print("Masukkan waktu tidur yang diinginkan (dalam jam): ");
        int waktuTidurDiinginkan = scanner.nextInt();

        aturWaktuTidur(waktuTidurDiinginkan);
    }

    private void aturWaktuTidur(int waktuTidurDiinginkan) {
        if (waktuTidurDiinginkan >= 7 && waktuTidurDiinginkan <= 9) {
            System.out.println("Waktu tidur Anda sudah cukup. Tetap jaga kesehatan!");
        } else if (waktuTidurDiinginkan < 7) {
            System.out.println("Anda mungkin memerlukan lebih banyak waktu tidur. Pertimbangkan untuk tidur lebih awal.");
        } else {
            System.out.println("Anda mungkin tidur terlalu banyak. Pertimbangkan untuk bangun lebih awal.");
        }
    }
}
