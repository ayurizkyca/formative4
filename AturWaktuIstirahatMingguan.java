import java.util.Scanner;

public class AturWaktuIstirahatMingguan {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Atur Waktu Istirahat Mingguan =====");
        System.out.print("Masukkan total waktu tidur per minggu (dalam jam): ");
        int waktuIstirahatMingguan = scanner.nextInt();

        aturWaktuIstirahatMingguan(waktuIstirahatMingguan);

        scanner.close();
    }

    private void aturWaktuIstirahatMingguan(int waktuIstirahatMingguan) {
        if (waktuIstirahatMingguan >= 49 && waktuIstirahatMingguan <= 56) {
            System.out.println("Waktu istirahat Anda per minggu sudah cukup. Tetap jaga kesehatan!");
        } else if (waktuIstirahatMingguan < 49) {
            System.out.println("Anda mungkin memerlukan lebih banyak waktu istirahat. Pertimbangkan untuk tidur lebih awal.");
        } else {
            System.out.println("Anda mungkin tidur terlalu banyak. Pertimbangkan untuk bangun lebih awal.");
        }
    }
}
