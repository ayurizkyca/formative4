import java.util.Scanner;

public class CatatDurasiLatihan {

    void run() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("===== Catat Durasi Olahraga =====");
        System.out.print("Masukkan durasi olahraga (menit): ");
        int durasiOlahraga = scanner.nextInt();

        if (isNormal(durasiOlahraga)) {
            tampilkanHasilDurasiOlahraga(durasiOlahraga);
        } else {
            System.out.println("Durasi Olahraga Anda Tidak Normal");
        }

    }

    private boolean isNormal(int durasiOlahraga) {
        return (durasiOlahraga >= 30 && durasiOlahraga <= 60);
    }

    private void tampilkanHasilDurasiOlahraga(int jarakTempuh) {
        System.out.println("===== Menyimpan Durasi Olahraga =====");
        System.out.println("Durasi Olahraga: " + jarakTempuh);
        System.out.println("Olahraga Anda Normal, Pertahankan!");
    }
}
